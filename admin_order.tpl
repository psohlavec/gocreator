<fieldset>
        <legend><img src="../img/admin/photo.gif"> GoCreator - data pro grafika</legend>
 
        {if isset($attachments['work']) }
        <p>Klient objednal smazání pozadí u <strong>{$attachments['work'][0]['source']}</strong> obrázků</p>
        {/if}
        
        <hr style="border-bottom:1px dotted #aaa;" />
        
        <h4><strong>Náhledy designu</strong></h4>
        
        {if $attachments && isset($attachments['thumb']) }
        
                {foreach $attachments['thumb'] as $thumb}
                
                <a rel="group_thumbs" class="fancybox" href="{$thumb['source']}"><img width="80" src="{$thumb['source']}" />   </a>
                
                {/foreach}
        
                {else}
        
                <p>Žádné náhledy</p>
                
        {/if}
        
        <h4><strong>Fonty</strong></h4>
        
        {if $attachments && isset($attachments['font']) }
        
                <ul>
                    {foreach $attachments['font'] as $font}
                
                    <li><a target="_blank" style="text-decoration: underline;" href="https://www.google.com/fonts/specimen/{$font['source']}">{$font['source']}</a></li>
                        
                    {/foreach}    
                </ul>
        
                {else}
                
                <p>Žádné fonty</p>
        
        {/if}
        
        <h4><strong>Obrázky</strong></h4>
        
        
        {if $attachments && isset($attachments['photo']) }
        
                {foreach $attachments['photo'] as $photo}
                
                <a rel="group_photos" class="fancybox" href="{$photo['source']}"><img height="40" src="{$photo['source']}" />   </a>
                
                {/foreach}
        
                {else}
                
                <p>Žádné fotky</p>
        
        {/if}
       
        
</fieldset>

<script>
        $(".fancybox").fancybox();
</script>