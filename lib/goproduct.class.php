<?php

class goProduct {
    
    // Presta objekt produktu
    public $product;
    
    // Prestashop context controller
    public $context;
    
    // Cena produktu
    public $price;
    
    // Typ produktu ( tričko / vak)
    public $type;
    
    // Název produktu
    public $name;
    
    // Cesta k obrázku do presty
    public $thumbPath;
    
    public function __construct($price, $type, $name, $imagePath) {
         
        $this->context = $context = Context::getContext();
        
        // věci poslané z creatoru - zatím na pevno
        $this->price = $price;
        $this->type = $type;
        $this->thumbPath = $imagePath;
        $this->name = $name;
  
    }
    
    public function saveThumbs($idCreatorProduct, $images) {
        
        foreach($images as $imageURL) {
            
            Db::getInstance()->insert('creator_attachment', array(
                'id_creator_product' => $idCreatorProduct,
                'type' => 'thumb',
                'source' => $imageURL
            ),false,true,Db::INSERT,false);
            
        }
        
    }
    
    public function saveParam($idCreatorProduct, $paramName, $value) {
        
        Db::getInstance()->insert('creator_attachment', array(
                'id_creator_product' => $idCreatorProduct,
                'type' => $paramName,
                'source' => $value
        ),false,true,Db::INSERT,false);
        
    }
    
    public function saveProduct() {
        
        $id = Db::getInstance()->insert('creator_product', array(
            'id_product' => (int)$this->product->id,
            'price'      => round($this->price,2),
        ),false,true,Db::INSERT,false);
        
    
        return Db::getInstance()->Insert_ID();

    }
    
    
    public function insertImages() {
        
        $image = new Image();
        $image->id_product = intval($this->product->id);
        $image->position = Image::getHighestPosition($this->product->id) + 1;
        $image->cover = true;
        $id_image = $image->id;
        $image->add();
        $tmpName = tempnam(_PS_IMG_DIR_, 'PS');
        
        if(strpos($this->thumbPath, 'ttp://')) {
         
            $tempPath = './temp/'.uniqid();
            
            if(file_put_contents($tempPath, fopen($this->thumbPath, 'r'))) {
                $this->thumbPath = $tempPath;
            } else {
                echo 'Image download error';exit;
            }
            
            
        }
    
        copy($this->thumbPath, $tmpName);
        
        $new_path = $image->getPathForCreation();
        ImageManager::resize($tmpName, $new_path.'.'.$image->image_format);
        $imagesTypes = ImageType::getImagesTypes('products');
        foreach ($imagesTypes as $imageType) {
            ImageManager::resize($tmpName, $new_path.'-'.stripslashes($imageType['name']).'.'.$image->image_format, $imageType['width'], $imageType['height'], $image->image_format);
        }

        
    }
    
    public function insertAttributes($options, $attrNames) {
        
        $tab = array_values($options);
        

        if (sizeof($tab) AND  Validate::isLoadedObject($this->product))
        {
        
            $attributes = array();
        
            foreach ($tab AS $group)
                    foreach ($group AS $attribute)
                            $attributes[] = '('.(int)($this->product->id).', '.(int)($attribute).', '.(float)($_POST['price_impact_'.(int)($attribute)]).', '.(float)($_POST['weight_impact'][(int)($attribute)]).')';
            
            Db::getInstance()->Execute(
                'INSERT INTO `'._DB_PREFIX_.'attribute_impact` (`id_product`, `id_attribute`, `price`, `weight`)
                VALUES '.implode(',', $attributes).'
                ON DUPLICATE KEY UPDATE `price`=VALUES(price), `weight`=VALUES(weight)'
            );
        
        
            $combinations = array_values(self::createCombinations($tab));
            $values = array_values(array_map(array($this,'addAttribute'), $combinations));
            
            $this->product->deleteProductAttributes();
            $res = $this->product->addProductAttributeMultiple($values);
            $this->product->addAttributeCombinationMultiple($res, $combinations);
            
           
        } else {
            echo Tools::displayError('Unable to initialize parameters, combinations is missing or object cannot be load.');exit;
        }
        
        return $res;

    }
    
    public function generateOptions($attrNames) {
        
        switch($this->type) {
            
            case 'shirt':

                $options = array(
                    $attrNames['attrGroupTypeId'] =>
                        array(
                                $attrNames['attrIdPanske'] =>  $attrNames['attrIdPanske'],
                                $attrNames['attrIdDamske'] =>  $attrNames['attrIdDamske']
                            ),
                    $attrNames['attrGroupSizeId'] => array(
                                $attrNames['attrIdS'] =>  $attrNames['attrIdS'],
                                $attrNames['attrIdM'] =>  $attrNames['attrIdM'],
                                $attrNames['attrIdL'] =>  $attrNames['attrIdL'],
                                $attrNames['attrIdXL']=>  $attrNames['attrIdXL'],
                                $attrNames['attrIdXXL'] =>  $attrNames['attrIdXXL']
                        )
                );
                
            break;
            
            default: $options = null; break;
        }
        
        return $options;
    
    }
    
    public function createProduct() {
        
        $langId = (int) (Configuration::get('PS_LANG_DEFAULT'));
       
        $product = new Product();
        $product->name = array($langId => sprintf('%s - Vlastný design', $this->name));
        $product->price = $this->price;
        $product->wholesale_price = Tools::getValue('wholesale_price');
        $product->active = true;
        $product->available_for_order = true;
        $product->visibility = 'none';
        $product->is_virtual = false;
        $product->cache_has_attachments = true;
        $product->id_category_default=3;
        $product->link_rewrite = array($langId => 'vlastny-design-produktu');
        $product->add();
        $product->addToCategories(array(Configuration::get('CREATOR_PRODUCTS_CATEGORY')));
        
        
        $this->product = $product;
      
        return $product;
        
       
    }
    
    public function getQuantities($attrNames) {

        switch($this->type) {
            
            case 'bag':
                $qty = (isset($_REQUEST['qty_universal'])) ? $_REQUEST['qty_universal'] : 1;
            break;
        
            case 'shirt':
                
                // v případě trička
                $qty = array(
                    $attrNames['attrIdPanske'] =>
                        array(
                                $attrNames['attrIdS'] => (isset($_REQUEST['m_size_s'])) ? (int)$_REQUEST['m_size_s'] : 0,
                                $attrNames['attrIdM'] => (isset($_REQUEST['m_size_m'])) ? (int)$_REQUEST['m_size_m'] : 0,
                                $attrNames['attrIdL'] => (isset($_REQUEST['m_size_l'])) ? (int)$_REQUEST['m_size_l'] : 0,
                                $attrNames['attrIdXL']=> (isset($_REQUEST['m_size_xl'])) ? (int)$_REQUEST['m_size_xl'] : 0,
                                $attrNames['attrIdXXL'] => (isset($_REQUEST['m_size_xxl'])) ? (int)$_REQUEST['m_size_xxl'] : 0,
                            ),
                    $attrNames['attrIdDamske'] => array(
                                $attrNames['attrIdS'] => (isset($_REQUEST['w_size_s'])) ? (int)$_REQUEST['w_size_s'] : 0,
                                $attrNames['attrIdM'] => (isset($_REQUEST['w_size_m'])) ? (int)$_REQUEST['w_size_m'] : 0,
                                $attrNames['attrIdL'] => (isset($_REQUEST['w_size_l'])) ? (int)$_REQUEST['w_size_l'] : 0,
                                $attrNames['attrIdXL']=> (isset($_REQUEST['w_size_xl'])) ? (int)$_REQUEST['w_size_xl'] : 0,
                                $attrNames['attrIdXXL'] => (isset($_REQUEST['w_size_xxl'])) ? (int)$_REQUEST['w_size_xxl'] : 0,
                        )
                );

            break;
            
        }
        
        return $qty;
 
    }
    
    public static function getAttrNames() {

        return array(

            'attrGroupTypeId' =>  Configuration::get('CREATOR_GROUP_TYPE_ID'),
            'attrIdPanske' => Configuration::get('CREATOR_ATTR_PANSKE_ID'),
            'attrIdDamske' => Configuration::get('CREATOR_ATTR_DAMSKE_ID'),
            
            'attrGroupSizeId' => Configuration::get('CREATOR_GROUP_SIZE_ID'),
            'attrIdS' => Configuration::get('CREATOR_ATTR_S_ID'),
            'attrIdM' => Configuration::get('CREATOR_ATTR_M_ID'),
            'attrIdL' => Configuration::get('CREATOR_ATTR_L_ID'),
            'attrIdXL'=> Configuration::get('CREATOR_ATTR_XL_ID'),
            'attrIdXXL' => Configuration::get('CREATOR_ATTR_XXL_ID')
        
        );
        
        
    }
    
    
    private function addAttribute($attributes, $price = 0, $weight = 0) {
            foreach ($attributes as $attribute)
            {
                    $price += (float)preg_replace('/[^0-9.]/', '', str_replace(',', '.', Tools::getValue('price_impact_'.(int)$attribute)));
                    $weight += (float)preg_replace('/[^0-9.]/', '', str_replace(',', '.', Tools::getValue('weight_impact_'.(int)$attribute)));
            }
            if ($this->product->id)
            {
                    return array(
                            'id_product' => (int)$this->product->id,
                            'price' => (float)$price,
                            'weight' => (float)$weight,
                            'ecotax' => 0,
                            'quantity' => 0,
                            'reference' => NULL,
                            'default_on' => 0,
                            'available_date' => '0000-00-00'
                    );
            }
            return array();
    }
    
    private static function createCombinations($list) {
    
    if (sizeof($list) <= 1)
            return sizeof($list) ? array_map(create_function('$v', 'return (array($v));'), $list[0]) : $list;
        $res = array();
        $first = array_pop($list);
        foreach ($first AS $attribute)
        {
                $tab = self::createCombinations($list);
                foreach ($tab AS $toAdd)
                        $res[] = is_array($toAdd) ? array_merge($toAdd, array($attribute)) : array($toAdd, $attribute);
        }
        return $res;
        
    }
    
}