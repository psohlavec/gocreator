<?php

require('../../config/config.inc.php');
require('./lib/goproduct.class.php');

/************************************************
 *    Kontrola vstupu
 ***********************************************/

if(!isset($_REQUEST['images']) ||
    !isset($_REQUEST['price']) ||
    !isset($_REQUEST['name']) ||
    !isset($_REQUEST['fonts']) ||
    !isset($_REQUEST['photos']) ||
    !isset($_REQUEST['type'])
   ) {
    echo 'Input error. Missing variables.';exit;
}

$images = json_decode($_REQUEST['images']);
$fonts = array();
$photos = array();


if(json_last_error() != JSON_ERROR_NONE || count($images) < 1) {
    echo 'Input error. Missing images data';exit;
}

if(strlen($_REQUEST['fonts'])) {

    $fonts = json_decode($_REQUEST['fonts']);
    if(json_last_error() != JSON_ERROR_NONE || count($fonts) < 1) {
        echo 'Input error. Wrong fonts input data';exit;
    }

}
if(strlen($_REQUEST['photos'])) {

    $photos = json_decode($_REQUEST['photos']);
    if(json_last_error() != JSON_ERROR_NONE || count($photos) < 1) {
        echo 'Input error. Wrong fonts input data';exit;
    }

}


$mainImagePath = $images[0];

// @todo - kontrola ceny
$price = $_REQUEST['price'];

// @todo - kontrola validty typu
$type = $_REQUEST['type'];

// @todo - kontrola názvu
$name = $_REQUEST['name'];


/************************************************
 *    Vložení produktu
 ***********************************************/

// vytvoříme objekt produktu


$p = new goProduct($price, $type, $name, $mainImagePath);


// načteme idčka atributů z konfigurace
$attrNames = goProduct::getAttrNames();


// načteme počty produktů
$quantities = $p->getQuantities($attrNames);


// vytvoříme presta produkt
$p->createProduct();


// uložíme produkt do vlastní tabulky s ps_id_product
$idCreatorProduct = $p->saveProduct();

// vygenerujeme pole idček atributů 
$options = $p->generateOptions($attrNames);

// vložíme atributy do databáze & spárujeme
if(is_array($options) && count($options) > 0) {
    $attributes = $p->insertAttributes($options, $attrNames);
}

// uložíme obrázky
$p->insertImages();


// uložíme přílohy (@ zatím thumbnaily)
$p->saveThumbs($idCreatorProduct, $images);


/*
 * Parametry creator_attachment
 *
 */

// odstranění pozadí?
if(isset($_POST['background'])) {
    $p->saveParam($idCreatorProduct, 'background', '1');
}

if(count($fonts) > 0 ) {

    foreach($fonts as $font) {
        $p->saveParam($idCreatorProduct, 'font', $font);
    }

}

if(count($photos) > 0 ) {

    foreach($photos as $photo) {
        $p->saveParam($idCreatorProduct, 'photo', $photo);
    }

}


if(isset($_POST['work']) && is_numeric($_POST['work']) && $_POST['work'] > 0) {
    
    $works = (int)$_POST['work'];
    $p->saveParam($idCreatorProduct, 'work', $works);

}


/************************************************
 *    Vložíme do košíku
 ***********************************************/

// vytvoříme košík
$idCart = $p->context->cookie->__get('id_cart');

if (!empty($idCart)) {
    $cart = new Cart($idCart);
}
if (!isset($cart) OR !$cart->id) {
    $cart = new Cart();
    $cart->id_customer = (int)($p->context->cookie->id_customer);
    $cart->id_lang = (int)($p->context->cookie->id_lang);
    $cart->id_currency = (int)($p->context->cookie->id_currency);
    $cart->add();
    $p->context->cookie->id_cart = (int)($cart->id);    
}


// přidání odstranění pozadí do košíku
if(isset($works)) {
    $cart->updateQty($works, Configuration::get('CREATOR_WORK_PRODUCT_ID'));
}

// pokud produkt nemá atributy, prostě vložíme počet ...
if(!isset($attributes)) {
    
    $cart->updateQty($quantities, $p->product->id);
    
    echo '<p>Pridano '.$quantities.'ks '.$p->name.'</p>';
    

// jinak projdeme pole atributů a díváme se jestli je nějaký vybraný
} else {
    
    foreach($attributes as $attributeId) {
        
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'product_attribute_combination`
                            WHERE id_product_attribute = '.(int)$attributeId;
        
        $result = Db::getInstance()->executeS($sql);
        
        $attrSize = new Attribute($result[0]['id_attribute']);
        $attrType = new Attribute($result[1]['id_attribute']);

        $attrData = array(
                            'size' => $attrSize->name[(int)$p->context->cookie->id_lang],
                            'type' => $attrType->name[(int)$p->context->cookie->id_lang]
                        );
        

        if(isset($quantities[$attrType->id]) &&
            isset($quantities[$attrType->id][$attrSize->id]) &&
            (int)$quantities[$attrType->id][$attrSize->id] > 0
        )
        {
        
            $qty = $quantities[$attrType->id][$attrSize->id];
            $cart->updateQty($qty, $p->product->id, $attributeId);
            
            // log
            echo '<p>Pridano '.$qty.'ks '.$p->name.' - '.$attrData['size'].' '.$attrData['type'].'</p>';
            
            
        } 
    
    }

}

// Redirect
header("HTTP/1.1 301 Moved Permanently"); 
header('location:http://dev.gotriko.sk/objednavka');
exit;


