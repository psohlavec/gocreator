<?php

include_once(_PS_MODULE_DIR_.'gosmsky/gocreator.class.php');

class gocreator extends Module
{
	protected $html = '';
	
	public function __construct()
	{
		$this->name = 'gocreator';
		$this->tab = 'others';
		$this->version = '0.3';
		$this->author = 'hoxpe';

		parent::__construct();
		
		$this->groups = AttributeGroup::getAttributesGroups($this->context->language->id);

		$this->displayName = $this->l('GoCreator');
		$this->description = $this->l('Administrace & skripty creatoru');
	}

	public function install()
	{                               

		if (!parent::install() ||
			!$this->registerHook('actionOrderStatusPostUpdate') ||
			!$this->registerHook('actionValidateOrder') ||
			!$this->registerHook('displayAdminOrder'))
			return false;
		
	
		// tabulka custom produkty
		Db::getInstance()->query('CREATE TABLE IF NOT EXISTS creator_product (
					id_creator_product INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
					id_order INT DEFAULT NULL,
					id_product INT,
					price FLOAT,
					creation_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
					)');
		
		// tabulka na přílohy
		Db::getInstance()->query('CREATE TABLE IF NOT EXISTS creator_attachment (
					id_creator_attachment INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
					id_creator_product INT,
					type ENUM("thumb","photo","pdf","font","work","background"),
					source VARCHAR(256)
					)');

		return true;
	}

	public function uninstall()
	{
		if (!parent::uninstall())
			return false;
		return true;
	}

	private function _postProcess()
	{
		if (Tools::isSubmit('submitSettings'))
		{
			
			Configuration::updateValue('CREATOR_WORK_PRODUCT_ID', Tools::getValue('work_product_id'));
			
			
			Configuration::updateValue('CREATOR_GROUP_TYPE_ID', Tools::getValue('group_type_id'));
			Configuration::updateValue('CREATOR_ATTR_PANSKE_ID', Tools::getValue('attr_panske_id'));
			Configuration::updateValue('CREATOR_ATTR_DAMSKE_ID', Tools::getValue('attr_damske_id'));
			
			Configuration::updateValue('CREATOR_GROUP_SIZE_ID', Tools::getValue('group_size_id'));
			Configuration::updateValue('CREATOR_ATTR_S_ID', Tools::getValue('attr_s_id'));
			Configuration::updateValue('CREATOR_ATTR_M_ID', Tools::getValue('attr_m_id'));
			Configuration::updateValue('CREATOR_ATTR_L_ID', Tools::getValue('attr_l_id'));
			Configuration::updateValue('CREATOR_ATTR_XL_ID', Tools::getValue('attr_xl_id'));
			Configuration::updateValue('CREATOR_ATTR_XXL_ID', Tools::getValue('attr_xxl_id'));
			
			Configuration::updateValue('CREATOR_PRODUCTS_CATEGORY', Tools::getValue('products_category'));
			
			
			$this->html .= $this->displayConfirmation($this->l('Aktualizováno'));
		}
	}
	
	
	private function getValueSelectHtml($name, $key, $groupId) {
		
		$values = AttributeGroup::getAttributes($this->context->language->id, $groupId);
	
		$html = '<select name="'.$name.'">';
		foreach($values as $val) {
			$html.='<option value="'.$val['id_attribute'].'"';
			if($val['id_attribute'] == Configuration::get($key)) {
				$html .= 'selected="selected"';
			}
			$html .= '>'.$val['name'].'</option>'.PHP_EOL;
		}
		$html .= '</select>';

		return $html;
		
		
	}
	
	private function getSelectHtml($name, $key) {
	
		$html = '<select name="'.$name.'">';
	
		foreach($this->groups as $group) {
			$html.='<option value="'.$group['id_attribute_group'].'"';
			if($group['id_attribute_group'] == Configuration::get($key)) {
				$html .= 'selected="selected"';
			}
			$html .= '>'.$group['public_name'].'</option>'.PHP_EOL;
		}
		$html .= '</select>';

		return $html;
		
	}
	
	private function getCategoriesSelect() {
	
		$html = '<select name="products_category">';
	
		$categories = Category::getSimpleCategories($this->context->language->id);

		foreach($categories as $category) {

			$html.='<option value="'.$category['id_category'].'"';
			if($category['id_category'] == Configuration::get('CREATOR_PRODUCTS_CATEGORY')) {
				$html .= 'selected="selected"';
			}
			$html .= '>'.$category['name'].'</option>'.PHP_EOL;
		}
		$html .= '</select>';
		
		
		return $html;
		
	}
	
	public function getContent()
	{
		$this->_postProcess();
		
		$this->html .= '
			<h2>'.$this->l('GoCreator&copy; by TB & PH').'</h2>
			<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">
				<fieldset>
					<legend>Dev konfigurace - nesahat</legend>

					<label>'.$this->l('Kategorie pro vlastní produkty').'</label>
					<div class="margin-form">
					'.$this->getCategoriesSelect().'
					</div>
					
					
					<label>'.$this->l('Kategorie pro vlastní produkty').'</label>
					<div class="margin-form">
					<input type="number" name="work_product_id" value="'.Configuration::get('CREATOR_WORK_PRODUCT_ID').'" /> 
					</div>

					<hr style="border-bottom:1px dotted #aaa;" />

					<label>'.$this->l('Atribut - Typ trička').'</label>
					<div class="margin-form">
					'.$this->getSelectHtml('group_type_id','CREATOR_GROUP_TYPE_ID').'
					</div>
					
					<label>'.$this->l('Hodnota - Pánské').'</label>
					<div class="margin-form">
					'.$this->getValueSelectHtml('attr_panske_id', 'CREATOR_ATTR_PANSKE_ID', Configuration::get('CREATOR_GROUP_TYPE_ID')).'
					</div>
					
					<label>'.$this->l('Hodnota - Dámské').'</label>
					<div class="margin-form">
					'.$this->getValueSelectHtml('attr_damske_id', 'CREATOR_ATTR_DAMSKE_ID',Configuration::get('CREATOR_GROUP_TYPE_ID')).'
					</div>
					
					<hr style="border-bottom:1px dotted #aaa;" />
					
					
					<label>'.$this->l('Atribut - Velikost').'</label>
					<div class="margin-form">
					'.$this->getSelectHtml('group_size_id','CREATOR_GROUP_SIZE_ID').'
					</div>
					
					<label>'.$this->l('Hodnota - S').'</label>
					<div class="margin-form">
					'.$this->getValueSelectHtml('attr_s_id', 'CREATOR_ATTR_S_ID',Configuration::get('CREATOR_GROUP_SIZE_ID')).'
					</div>
					
					<label>'.$this->l('Hodnota - M').'</label>
					<div class="margin-form">
					'.$this->getValueSelectHtml('attr_m_id', 'CREATOR_ATTR_M_ID',Configuration::get('CREATOR_GROUP_SIZE_ID')).'
					</div>
					
					<label>'.$this->l('Hodnota - L').'</label>
					<div class="margin-form">
					'.$this->getValueSelectHtml('attr_l_id', 'CREATOR_ATTR_L_ID',Configuration::get('CREATOR_GROUP_SIZE_ID')).'
					</div>
					
					<label>'.$this->l('Hodnota - XL').'</label>
					<div class="margin-form">
					'.$this->getValueSelectHtml('attr_xl_id', 'CREATOR_ATTR_XL_ID',Configuration::get('CREATOR_GROUP_SIZE_ID')).'
					</div>
					
					<label>'.$this->l('Hodnota - XXL').'</label>
					<div class="margin-form">
					'.$this->getValueSelectHtml('attr_xxl_id', 'CREATOR_ATTR_XXL_ID',Configuration::get('CREATOR_GROUP_SIZE_ID')).'
					</div>

					<div class="clear" style="margin-top: 20px"></div>
					<div class="clear">
						<label></label>
						<input type="submit" style="margin-top:20px;padding:10px 20px;font-size:13px;" name="submitSettings" value="'.$this->l('   Uložit   ').'" class="button" />
					</div>
				
				</fieldset>
			</form>
		
		';

		return $this->html;
	}

	public function hookActionValidateOrder($params) {
		
		if(isset($params['order']) &&
		   Validate::isLoadedObject($params['order']) &&
		   isset($params['cart']) &&
		   Validate::isLoadedObject($params['cart'])
		   ) {
		
			$order = $params['order'];
			$cart = $params['cart'];
			echo $order->id;
			
			$products = $cart->getProducts();
			
			if(is_array($products) && count($products) > 0) {
				
				foreach($products as $product) {
					
					$idProduct = $product['id_product'];
					
					$creatorProducts = Db::getInstance()->executeS('SELECT * FROM creator_product WHERE id_product = '. $idProduct);
				
	
					if($creatorProducts) {
					
						foreach($creatorProducts as $creatorProduct) {
							
							Db::getInstance()->execute('UPDATE creator_product SET id_order = '.$order->id.' WHERE id_creator_product = '.$creatorProduct['id_creator_product']);
							
						}
						
					
					} else {
						echo 'produkt nenalezen v creator_product - není custom';
					}
				
				}
			
			}
					
			
		} else die('invalidni data');
		

	}
	
	public function hookDisplayAdminOrder($params) {

		
		if($designs = Db::getInstance()->executeS('SELECT * FROM creator_product WHERE id_order = '. Tools::getValue('id_order'))) {
		
			
			$clause = '';
			
			foreach($designs as $key => $design) {
				$clause .= 'id_creator_product = '. $design['id_creator_product'];
				if($key != count($designs) -1)
					$clause .= ' OR ';
			}
			
	
			$attachments = Db::getInstance()->executeS('SELECT * FROM creator_attachment WHERE 1 AND ('.$clause.')');

			
			if($attachments) {
				
				$temp = array();
				
				foreach($attachments as $attachment) {
					
					if(array_key_exists($attachment['type'], $temp)) {
						$temp[$attachment['type']][] = $attachment;
					} else {
						$temp[$attachment['type']] = array($attachment);
					}
	
				}
				
				$attachments = $temp;
				
			}
					
			$this->smarty->assign(array(
				'time' => $design['creation_time'],
				'attachments' => $attachments,
			));
		
			return $this->display(__FILE__, 'admin_order.tpl');
		
		} else return;
	
	}
	


}
